version = '23.05.04'
devel = False

def get_name():
    return 'Revumatic'

def get_desc():
    return 'Red Hat code review tool'

def get_version():
    if devel:
        return 'v{}+dev (development version)'.format(version)
    return 'v{}'.format(version)
