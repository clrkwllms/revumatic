import enum
import io
import Levenshtein
import re
from itertools import zip_longest

class EncapsulatedList:
    def __init__(self):
        self._list = []

    def __len__(self):
        return len(self._list)

    def __getitem__(self, key):
        return self._list[key]

    def __iter__(self):
        return iter(self._list)

    def append(self, item):
        self._list.append(item)


class Patch(EncapsulatedList):
    def __init__(self, identifier):
        super().__init__()
        self.identifier = identifier

    def postprocess(self):
        self._list.sort()
        if len(self._list) > 0:
            self.max_changes = max(self._list, key=lambda x: x.changes).changes
        else:
            self.max_changes = 0


    def debug_format(self, width=79):
        def out(left, right, sep='|', replace_left=None, replace_right=None):
            if replace_right is None:
                replace_right = replace_left
            left = '' if not left else (left if replace_left is None else replace_left)
            right = '' if not right else (right if replace_right is None else replace_right)
            left = left.replace('\t', '        ')
            right = right.replace('\t', '        ')
            result.append('{:{w}.{w}} {} {:{w}.{w}}'.format(left, sep, right, w=w))

        result = []
        w = (width - 3) // 2
        if self.was_compared():
            for f in self.compared():
                out(f.left, f.right,
                    replace_left='\n'.join(f.headers),
                    replace_right='\n'.join(f.other_file.headers) if f.other_file else None)
                for h in f.compared():
                    sep = 'X' if h.left and h.left.modified else '|'
                    out(h.left.header if h.left else None, h.right.header if h.right else None,
                        sep=sep)
                    for i, j in zip_longest(h.left if h.left else (), h.right if h.right else ()):
                        out(i.origin + i.content if i else None,
                            j.origin + j.content if j else None,
                            sep=sep)
        else:
            for f in self:
                result.extend(f.headers)
                for h in f:
                    result.append(h.header)
                    for line in h:
                        result.append(line.origin + line.content)
        return '\n'.join(result)


    def get_line(self, path, line_no, old, count):
        """Returns the given line and up to (count-1) previous lines. The
        line is identified by the path and line number. If old == True, the
        old path and removed line numbers are considered, if old == False,
        the new path and added line numbers are used. Returns None if no
        such line could be found."""
        for f in self:
            if (old and f.name == path) or (not old and f.new_name == path):
                break
        else:
            return None
        stat_index = 0 if old else 1
        for h in f:
            start = h.stat[stat_index]
            end = h.stat[stat_index + 2] + start
            if line_no >= start and line_no < end:
                break
        else:
            return None
        last = None
        for i, line in enumerate(h):
            if (old and line.old_pos == line_no) or (not old and line.new_pos == line_no):
                last = i
            elif last is not None:
                # no need to iterate further, we got the last matching line
                break
        return h[max(0, last - count + 1) : last + 1]


    def compare(self, other):
        """Compares this patch to 'other' patch, finding matching hunks and
        recording the info. Use the 'compared' method to get access to the
        result."""
        self._compared = None
        self._missing = []
        self.modified_hunks = 0
        self.extra_hunks = 0
        self.missing_hunks = 0
        self.context_diff = False
        i = j = 0
        while i < len(self) and j < len(other):
            if self[i] < other[j]:
                self.extra_hunks += self[i].mark_extra()
                i += 1
                continue
            if other[j] < self[i]:
                self._missing.append(other[j])
                self.missing_hunks += len(other[j])
                j += 1
                continue
            modified_hunks, extra_hunks, missing_hunks, context = self[i].compare(other[j])
            self.modified_hunks += modified_hunks
            self.extra_hunks += extra_hunks
            self.missing_hunks += missing_hunks
            if context:
                self.context_diff = True
            i += 1
            j += 1
        while i < len(self):
            self.extra_hunks += self[i].mark_extra()
            i += 1
        while j < len(other):
            self._missing.append(other[j])
            self.missing_hunks += len(other[j])
            j += 1


    def was_compared(self):
        """Returns True if the patch was previously compared with another
        patch."""
        return hasattr(self, '_compared')


    def compared(self):
        """Returns iterator over sorted files after patch comparison. Each
        file contains a 'left' and 'right' boolean attributes denoting
        whether it should be displayed on left side and/or right side. To
        access hunks on an individual file, call its 'compared' method.
        If the patch was not compared before, the result is undefined."""
        if self._compared is None:
            self._compared = [ComparedProxy(f, left=True, right=not f.is_extra) for f in self]
            self._compared.extend(ComparedProxy(f, left=False, right=True) for f in self._missing)
            self._compared.sort()
        for c in self._compared:
            yield c


class File(EncapsulatedList):
    def __init__(self, file_id):
        super().__init__()
        self.added = 0
        self.removed = 0
        self.changes = 0
        self.name = None
        self.new_name = None
        self.file_id = file_id
        self.other_file = None


    def set_names(self, old_name, new_name):
        if old_name is not None:
            self.name = old_name
            self.old_null = old_name == '/dev/null'
        if new_name is not None:
            self.new_name = new_name
            self.new_null = new_name == '/dev/null'


    def append(self, hunk, added, removed):
        super().append(hunk)
        self.added += added
        self.removed += removed
        self.changes += added + removed


    def __eq__(self, other):
        return self.new_name == other.new_name and self.name == other.name

    def __lt__(self, other):
        one = self.new_name if self.old_null else self.name
        two = other.new_name if other.old_null else other.name
        if one != two:
            return one < two
        # Here, either the old_names are the same (which can legally happen
        # only if the files are equal), or we deleted/renamed a file and
        # created a new one with the same name. We want to sort the
        # deleted/renamed file earlier.
        if self.old_null:
            # Self is the new file. If other.old_null is also True, the
            # files are equal and we return False. Otherwise, other got
            # renamed or deleted and we want other to be sorted earlier.
            # Which means returning False.
            return False
        if other.old_null:
            # Other is the new file. Other thus comes after self.
            return True
        # If we reach this point, the files must be the same. Or it's
        # a malformed patch and we renamed the same file to two different
        # things. Handle both cases.
        return self.new_name < other.new_name

    def __le__(self, other):
        one = self.new_name if self.old_null else self.name
        two = other.new_name if other.old_null else other.name
        if one != two:
            return one < two
        if other.old_null:
            # If self.old_null is True, the files are equal and we return
            # True. Otherwise, see the comments above.
            return True
        if self.old_null:
            return False
        return self.new_name <= other.new_name


    def mark_extra(self):
        """Marks all hunks in this file as not being present in the patch
        we're being compared to."""
        self._missing = []
        self.is_extra = True
        for hunk in self:
            hunk.link = None
            hunk.modified = False
        return len(self)


    def compare(self, other):
        self.other_file = other
        self.is_extra = False
        modified_hunks = 0
        extra_hunks = 0
        context_diff = False
        # Find Levenshtein distances between each pair of (left hunk, right
        # hunk). Store them as a list of weighted edges, i.e. list of tuples
        # (distance, left hunk index, right hunk index). Do not store edges
        # with a weight larger than a threshold.
        distances = []
        for i, h1 in enumerate(self):
            for j, h2 in enumerate(other):
                h1_added = h1.lines_added
                h1_removed = h1.lines_removed
                h2_added = h2.lines_added
                h2_removed = h2.lines_removed
                # A (rather arbitrary) threshold for distance acceptance. We
                # accept changes up to 50% of the characters on added+removed
                # lines.
                threshold = max(len(h1_added) + len(h1_removed),
                                len(h2_added) + len(h2_removed)) // 2
                dist = Levenshtein.distance(h1_added, h2_added) + \
                       Levenshtein.distance(h1_removed, h2_removed)
                if dist <= threshold:
                    distances.append((dist, i, j))
        # Sort the edges from the lowest weight to the highest weight.
        distances.sort(key=lambda x: x[0])
        # Use a greedy algorithm to pick up the pairs of hunks.
        used_self = [False] * len(self)
        used_other = [False] * len(other)
        for dist, i, j in distances:
            if used_self[i] or used_other[j]:
                continue
            used_self[i] = True
            used_other[j] = True
            self[i].link = other[j]
            if dist > 0:
                self[i].modified = True
                self[i].fuzz = True
                modified_hunks += 1
            else:
                self[i].modified = False
                self[i].fuzz = self[i].any_difference(other[j])
                if self[i].fuzz:
                    context_diff = True
        # Mark the unmatched left hunks.
        for i, u in enumerate(used_self):
            if not u:
                self[i].link = None
                self[i].modified = False
                extra_hunks += 1
        # Collect the unmatches right hunks.
        self._missing = [other[j] for j, u in enumerate(used_other) if not u]
        return modified_hunks, extra_hunks, len(self._missing), context_diff


    def compared(self):
        """Returns iterator over sorted hunks after patch comparison. Each
        hunk contains 'left' and 'right' attributes containing lines that
        should be displayed on left and right side, respectively. In case
        there's nothing to display on one side, the corresponding attribute
        is None.
        If this is called on a hunk of patch that was not compared before,
        the result is undefined."""
        if not hasattr(self, '_compared'):
            # We took care to not alter the other patch during comparison.
            # This means that for files present only in the other patch, we
            # store the corresponding File object in the '_missing' list
            # unaltered. That gives us a possibility to discover such File
            # objects: they don't have the 'is_extra' attribute. Hunks of
            # such files appear only on the right side.
            if hasattr(self, 'is_extra'):
                self._compared = [ComparedProxy(h, h.link) for h in self]
                self._compared.extend(ComparedProxy(None, h) for h in self._missing)
            else:
                self._compared = [ComparedProxy(None, h) for h in self]
            self._compared.sort()
        for h in self._compared:
            yield h


class Hunk(EncapsulatedList):
    def __init__(self):
        super().__init__()
        self._lines_added = None
        self._lines_removed = None
        self.counted = { '+': 0, '-': 0, ' ': 0, '\\': 0 }

    @property
    def lines_added(self):
        if self._lines_added is None:
            self._lines_added = '\n'.join(line.content for line in self if line.origin == '+')
        return self._lines_added

    @property
    def lines_removed(self):
        if self._lines_removed is None:
            self._lines_removed = '\n'.join(line.content for line in self if line.origin == '-')
        return self._lines_removed

    def any_difference(self, other):
        return any(l1.origin != l2.origin or l1.content != l2.content
                   for l1, l2 in zip(self, other))

    def append_line(self, origin, content):
        old_pos = self.stat[0] + self.counted[' '] + self.counted['-']
        new_pos = self.stat[1] + self.counted[' '] + self.counted['+']
        self.append(Line(origin, content, old_pos, new_pos))
        self.counted[origin] += 1

    def is_complete(self):
        counted_old = self.counted['-'] + self.counted[' ']
        counted_new = self.counted['+'] + self.counted[' ']
        if self.stat[2] == counted_old and self.stat[3] == counted_new:
            return True
        if self.stat[2] < counted_old or self.stat[3] < counted_new:
            raise MalformedPatch('Extra lines in a hunk')
        return False

    def __eq__(self, other):
        return self.stat == other.stat
    def __lt__(self, other):
        return self.stat < other.stat
    def __le__(self, other):
        return self.stat <= other.stat


class Line:
    def __init__(self, origin, content, old_pos, new_pos):
        self.origin = origin
        self.content = content
        self.old_pos = old_pos
        self.new_pos = new_pos


class ComparedProxy:
    def __init__(self, *args, **kwargs):
        if len(args) == 0 or len(args) > 2:
            raise TypeError('need one or two wrapped objects ({} given)'.format(len(args)))
        self._wrapped1 = args[0]
        self._wrapped2 = args[1] if len(args) > 1 else None
        self.left = self._wrapped1
        self.right = self._wrapped2
        for k, v in kwargs.items():
            setattr(self, k, v)

    def __getattr__(self, attr):
        if self._wrapped1 is not None:
            return getattr(self._wrapped1, attr)
        return getattr(self._wrapped2, attr)

    def __eq__(self, other):
        return self.__getattr__('__eq__')(other)
    def __lt__(self, other):
        return self.__getattr__('__lt__')(other)
    def __le__(self, other):
        return self.__getattr__('__le__')(other)


class MalformedPatch(Exception):
    pass


class State(enum.IntEnum):
    DESCRIPTION = enum.auto()
    FILE_HEADER = enum.auto()
    PRE_HUNK = enum.auto()
    HUNK = enum.auto()
    POST_HUNK = enum.auto()
    FINISHED = enum.auto()


class PatchParser:
    class _Eof(Exception):
        pass

    def __init__(self, identifier, data):
        self.identifier = identifier
        self.f = io.StringIO(data)
        self.line_no = 0
        self.backbuffer = None
        self.state = State.DESCRIPTION
        self.description = []
        self.file_id = 0
        self.patch = Patch(self.identifier)

    def error(self, msg):
        return MalformedPatch('Patch {}: Line {}: {} ({})'.format(self.identifier, self.line_no, msg,
                                                                  self.state.name))

    def _readline(self):
        if self.backbuffer is not None:
            line = self.backbuffer
            self.backbuffer = None
        else:
            line = self.f.readline()
            self.line_no += 1
        return line

    def readline(self):
        line = self._readline()
        if line == '':
            raise self._Eof()
        return line.rstrip('\n')

    def peakline(self):
        line = self._readline()
        self.backbuffer = line
        return line.rstrip('\n')

    def ensure_state(self, msg, *args):
        for a in args:
            if self.state == a:
                return
        raise self.error(msg)

    def strip_path(self, path):
        if not path:
            raise self.error('Missing path')
        if path[0] == '/':
            return path
        components = path.split('/', maxsplit=1)
        if len(components) == 1:
            raise self.error('Patch not in -p1 format')
        return components[1]

    def ship_file(self):
        if self.state > State.DESCRIPTION:
            self.ship_hunk()
            if self.file.name is None or self.file.new_name is None:
                raise self.error('Missing path')
            self.patch.append(self.file)
        self.file = File(self.file_id)
        self.file_id += 1
        self.state = State.FILE_HEADER

    def ship_hunk(self):
        if self.state > State.PRE_HUNK:
            if not self.check_hunk_complete():
                raise self.error('Incomplete hunk')
            self.file.append(self.hunk, self.hunk.counted['+'], self.hunk.counted['-'])
        self.hunk = Hunk()
        self.state = State.HUNK

    def check_hunk_complete(self):
        try:
            return self.hunk.is_complete()
        except MalformedPatch as e:
            raise self.error(str(e)) from None

    def parse_step(self, line):
        if line.startswith('Index: '):
            # expect a line containing '======' or a hunk start
            next_line = self.peakline()
            if next_line.startswith('--- ') or next_line.replace('=', '') == '':
                self.ensure_state('Invalid Index: header',
                                  State.DESCRIPTION, State.HUNK, State.POST_HUNK)
                self.ship_file()
                self.file.headers = [line]
                return
        if line.startswith('diff '):
            # find the diff kind
            words = line.split()
            if len(words) >= 3:
                kind = words[1]
                if kind.startswith('--'):
                    if kind == '--cc':
                        # We don't support merge request diffs. Treat the
                        # patch as empty.
                        self.ensure_state('Cannot mix diff types', State.DESCRIPTION)
                        raise self._Eof()
                    if kind != '--git':
                        raise self.error('Unsupported diff type ({})'.format(kind[2:]))
                    words = words[1:]
                if words[1].find('/') >= 0 and words[2].find('/') >= 0:
                    self.ensure_state('Invalid diff header',
                                      State.DESCRIPTION, State.FILE_HEADER,
                                      State.PRE_HUNK, State.HUNK, State.POST_HUNK)
                    self.ship_file()
                    self.file.headers = [line]
                    # Set the paths from the diff --git header. These do not
                    # necessarily correspond to the actual old/new paths,
                    # since /dev/null is not used in this header, but we
                    # will overwrite them later with the real paths. There
                    # are some cases where the diff --git header is the only
                    # place where the paths are present, though, such as
                    # when changing only file mode, so we need to parse this
                    # here.
                    self.file.set_names(self.strip_path(words[1]), self.strip_path(words[2]))
                    return
            # ignore lines starting with "diff" that do not look like
            # a diff command
        if line.startswith('--- ') and line.find('/') >= 0 and \
           (self.state != State.HUNK or self.check_hunk_complete()):
            # The lines starting with '--- ' may be removed lines that start
            # with '-- '. Ignore this marking while processing an incomplete
            # hunk.
            self.ensure_state('Invalid file header fromat',
                              State.DESCRIPTION, State.FILE_HEADER, State.HUNK, State.POST_HUNK)
            if self.state != State.FILE_HEADER:
                self.ship_file()
            old_path = self.strip_path(line[4:])
            line = self.readline()
            if not line.startswith('+++ '):
                raise self.error('Invalid file header format')
            new_path = self.strip_path(line[4:])
            self.file.set_names(old_path, new_path)
            self.state = State.PRE_HUNK
            return
        if self.state == State.FILE_HEADER:
            self.file.headers.append(line)
            if line.startswith('rename from '):
                self.file.set_names(line[12:], None)
            elif line.startswith('rename to '):
                self.file.set_names(None, line[10:])
            elif line.startswith('new file '):
                self.file.set_names('/dev/null', None)
            elif line.startswith('deleted file '):
                self.file.set_names(None, '/dev/null')
            elif line.startswith('Binary files '):
                m = re.match(r'Binary files (.*) and (.*) differ', line)
                if m:
                    self.file.set_names(self.strip_path(m.group(1)),
                                        self.strip_path(m.group(2)))
            return
        if line.startswith('@@ ') and self.state >= State.PRE_HUNK and self.state < State.FINISHED:
            self.ship_hunk()
            components = line.split(maxsplit=4)
            if len(components) < 4 or components[3] != '@@' or \
               not components[1].startswith('-') or not components[2].startswith('+'):
                raise self.error('Invalid hunk header format')
            src = components[1][1:].split(',', maxsplit=1)
            dst = components[2][1:].split(',', maxsplit=1)
            if len(src) == 1:
                src.append(1)
            if len(dst) == 1:
                dst.append(1)
            try:
                self.hunk.stat = (int(src[0]), int(dst[0]), int(src[1]), int(dst[1]))
            except (IndexError, ValueError):
                raise self.error('Invalid hunk header format') from None
            self.hunk.header = line
            return
        if self.state == State.HUNK:
            if not line or line[0] not in ('+', '-', ' ', '\\'):
                raise self.error('Garbage in patch content')
            if line[0] == ' ' and self.hunk.counted['\\']:
                raise self.error('Context line present after a no-newline indicator')
            self.hunk.append_line(line[0], line[1:])
            if self.check_hunk_complete():
                next_line = self.peakline()
                if next_line.startswith('\\'):
                    self.hunk.append_line(next_line[0], next_line[1:])
                    self.readline()
                self.state = State.POST_HUNK
            return
        if self.state == State.DESCRIPTION:
            self.description.append(line)
            return
        if self.state >= State.POST_HUNK:
            self.state = State.FINISHED
            return
        raise self.error('Garbage in patch content')

    def parse(self):
        try:
            while True:
                line = self.readline()
                self.parse_step(line)
        except self._Eof:
            pass
        self.ship_file()
        self.patch.description = '\n'.join(self.description).rstrip('\n')
        self.patch.postprocess()
        return self.patch
