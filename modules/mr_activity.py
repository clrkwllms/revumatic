import datetime
import tuimatic
import weakref

from modules import (
    formatters,
    gitlab,
    mr_ui,
    packages,
    ui,
)


diff_quote_formatter = formatters.DiffQuoteFormatter()


class ThreadNotFoundError(Exception):
    pass


class BaseActivityItem:
    major_sep = '─'
    minor_sep = '·'

    def __init__(self, timestamp):
        if isinstance(timestamp, str):
            timestamp = gitlab.parse_datetime(timestamp)
        self.timestamp = timestamp
        self.widget_ref = None

    def get_save_data(self):
        """Construct and return a dictionary of data to save to the comment
        storage."""
        raise NotImplementedError

    def is_blocking(self):
        return False

    def _get_widget(self):
        result = None
        if self.widget_ref is not None:
            result = self.widget_ref()
        return result

    def get_widget(self):
        """Returns a stored widget or constructs a new one. The widget is
        internally kept as a weakref."""
        result = self._get_widget()
        if result is None:
            result = tuimatic.TextPile(self.format())
            self.widget_ref = weakref.ref(result)
        return result

    def format(self):
        raise NotImplementedError

    def _new_sep(self, major=True, blocking=False):
        sep = self.major_sep if major else self.minor_sep
        if blocking:
            text = tuimatic.Text(['{}{} '.format(sep, sep),
                                ('desc-err', 'blocking'),
                                ' '],
                                wrap='clip', padding_char=sep)
        else:
            text = tuimatic.TextSeparator(sep)
        text.blocking = blocking
        return text

    def _main_widget(self, widget, system=False, always=True):
        widget.is_system = system
        widget.show_always = always
        widget.is_comment_editor = False

    def _create_text(self, text='', system=False, always=True):
        widget = tuimatic.Viewer(text)
        self._main_widget(widget, system, always)
        return widget


class CommentHandler(BaseActivityItem, metaclass=tuimatic.MetaSignals):
    # A signal to notify the Activity object about the widget change. The
    # signal has two parameters: the sending activity and a bool. If the
    # bool is True, the sending activity item itself should be deleted from
    # the activity list.
    signals = ['changed']

    def __init__(self, timestamp, repo):
        self.repo = repo
        self.commit_ref = None
        super().__init__(timestamp)

    def get_widget(self):
        result = self._get_widget()
        if result is None:
            result = super().get_widget()
            result.comment_handler = self
        return result

    def set_commit_ref(self, commit):
        self.commit_ref = weakref.ref(commit)

    def get_commit(self):
        if self.commit_ref is None:
            return None
        return self.commit_ref()

    def get_comment_editor(self, focus_widget=None):
        pile = self._get_widget()
        if not pile:
            return None
        last = pile.contents[-1][0]
        if last.is_comment_editor:
            return last.contents[-1][0]
        return None

    def show_commit_info(self, enabled):
        return

    def preview(self, need_sep):
        """Return a text widget with the comment preview or None to ignore
        this comment."""
        editor = self.get_comment_editor()
        if editor.draft.state:
            return None
        widgets = []
        if need_sep or editor.blocking.state:
            widgets.append(self._new_sep(blocking=editor.blocking.state))
        widgets.append(tuimatic.Viewer(editor.text))
        return tuimatic.TextPile(widgets)

    def submit(self, mr, new_item_callback, callback_args):
        """Submit the comment to GitLab. If a new activity item needs to be added
        instead of the comment, call new_item_callback(new_item, *callback_args).
        The call must happen before the current item (i.e. the comment editor)
        is deleted."""
        raise NotImplementedError

    def _create_comment_editor(self, inactive_map):
        if self.is_blocking():
            # the real meaning of the checkbox is "is blocking"; here we
            # just make the checkbox look reversed
            block = ui.ToggleCheckBox('resolve thread', state=True, reverse=True)
        else:
            block = ui.ToggleCheckBox('block the MR')
        draft = ui.ToggleCheckBox('do not submit')
        head = tuimatic.TextColumns([('pack', block), draft], dividechars=2)
        edit = mr_ui.DeletableEditor(multiline=True, undo=True)
        edit.comment_handler = self
        edit.blocking = block
        edit.draft = draft
        pile = mr_ui.HighlightedTextPile([head, edit], inactive_map)
        pile.is_system = False
        pile.show_always = True
        pile.is_comment_editor = True
        tuimatic.connect_signal(edit, 'remove', self._remove_comment_editor)
        return pile

    def create_comment_editor(self, focus_widget, inactive_map):
        raise NotImplementedError

    def _remove_comment_editor(self):
        pile = self._get_widget()
        editor = self.get_comment_editor()
        assert editor, str(self)
        del pile.contents[-1]
        del pile.contents[-1]
        if len(pile.contents) == 0:
            self.widget_ref = None
        # emit the 'changed' signal; the item is deleted by the signal
        # handler if the widget is empty
        tuimatic.emit_signal(self, 'changed', self, len(pile.contents) == 0)
        # mark the edit widget as deleted and trigger its 'changed' signal
        editor.deleted = True
        editor.set_text('')


class ThreadActivityItem(CommentHandler):
    def __init__(self, thread, repo):
        self._cache_is_blocking = None
        self.thread = thread
        self.thread_id = thread['id']
        super().__init__(thread['notes'][0]['created_at'], repo)

    def get_save_data(self):
        return { 'thread_id': self.thread_id }

    def __str__(self):
        return '{}({})'.format(self.__class__.__name__, self.thread_id)

    def is_blocking(self):
        # When a thread is created by a reply to a system note, the system
        # note has resolvable == False. In order to find out whether a given
        # thread is blocking, we need to iterate over the whole thread.
        if self._cache_is_blocking is None:
            self._cache_is_blocking = any(note['resolvable'] and not note['resolved']
                                          for note in self.thread['notes'])
        return self._cache_is_blocking

    def get_commit_id(self):
        return self.thread['notes'][0].get('commit_id')

    def get_commit_position(self):
        pos = self.thread['notes'][0]['position']
        return (pos['old_path'], pos['new_path'], pos['old_line'], pos['new_line'])

    def format(self):
        self.commit_info = None
        result = []
        blocking = self.is_blocking()
        skip_thread = False
        for i, note in enumerate(self.thread['notes']):
            if not skip_thread:
                try:
                    always = self.repo.pkg.filter_note(note, i == 0)
                except packages.PackageSkipThread:
                    always = False
                    skip_thread = True

            diff_quote = None
            if note['type'] == 'DiffNote' and i == 0:
                pos = note['position']
                cf = None
                if note.get('commit_id'):
                    try:
                        cf = diff_quote_formatter.get_commit_formatter(self.repo,
                                                                       note['commit_id'])
                    except KeyError:
                        # the commit is not available, fall back to a diff range
                        pass
                if cf is None:
                    cf = diff_quote_formatter.get_commit_formatter_from_range(self.repo,
                                    pos['base_sha'], pos['head_sha'])
                diff_quote = diff_quote_formatter.format(cf, self.get_commit(),
                                pos['old_path'], pos['new_path'],
                                pos['old_line'], pos['new_line'])
                self.commit_info = len(result) + 1

            result.append(self._new_sep(major=(i==0),
                                        blocking=(i==0) and blocking))
            result.append(self._create_single_note(note, diff_quote,
                                                   note['system'], always))
        return result

    def show_commit_info(self, enabled):
        if self.commit_info is None:
            return
        widget = self._get_widget()
        if not widget:
            return
        widget.contents[self.commit_info][0].set_enabled(1, enabled)

    def update_thread(self, thread):
        assert(self.thread_id == thread['id'])
        prev_blocking = self.is_blocking()
        self.thread = thread
        self._cache_is_blocking = None
        if self.is_blocking() != prev_blocking:
            pile = self.get_widget()
            if pile:
                pile.setitem(0, self._new_sep(blocking=not prev_blocking))

    def submit(self, mr, new_item_callback, callback_args):
        editor = self.get_comment_editor()
        if editor.draft.state:
            return False
        text = editor.text
        blocking = editor.blocking.state
        thread = mr.discussion(self.thread_id)
        if not text:
            # An empty comment. First, adjust the blocking state if
            # needed. Then delete the comment.
            result = False
            if blocking != self.is_blocking():
                thread.resolve(not blocking)
                self.update_thread(thread)
                result = True
            self._remove_comment_editor()
            return result
        note = thread.new_note(body=text)
        if note['resolved'] == blocking:
            thread.resolve(not blocking)
        self._replace_comment_editor(thread)
        self.update_thread(thread)
        return True

    def _create_single_note(self, note, diff_quote=None, system=False, always=True):
        result = []
        date = formatters.format_timestamp(note['created_at'])
        body = note['body'].strip('\n') + '\n'
        if note['system']:
            diff_quote = None
            result.append(('desc-sys', '{}, '.format(date)))
            # format some specific things specially
            if body.startswith('marked this merge request as '):
                body = body[29:].strip().strip('*')
                result.append(('desc-sys', 'marked as '))
                result.append(('desc-version', body))
                result.append(('desc-sys', '\n'))
            else:
                result.extend(formatters.format_name(note['author'], system=True))
                result.append(('desc-sys', ' {}'.format(body)))
        else:
            result.append(('desc-date', date))
            result.append(('desc', ', '))
            result.extend(formatters.format_name(note['author']))
            result.append(('desc', '\n'))
            if not diff_quote:
                result.append(('desc', body))
        if not diff_quote:
            return self._create_text(result, system, always)
        widget = tuimatic.TextPile([tuimatic.Viewer(result),
                                 mr_ui.DiffQuote(diff_quote),
                                 tuimatic.Viewer([('desc', body)])])
        self._main_widget(widget, system, always)
        return widget

    def create_comment_editor(self, focus_widget, inactive_map):
        edit = self._create_comment_editor(inactive_map)
        widget = self._get_widget()
        if widget:
            widget.append(self._new_sep(major=False))
            widget.append(edit)
        return edit.contents[1][0], None

    def _replace_comment_editor(self, thread):
        pile = self._get_widget()
        editor = self.get_comment_editor()
        assert editor
        pile.setitem(-1, self._create_single_note(thread['notes'][-1]))
        tuimatic.emit_signal(self, 'changed', self, False)
        # mark the edit widget as deleted and trigger its 'changed' signal
        editor.deleted = True
        editor.set_text('')


class NewThreadActivityItem(CommentHandler):
    def __init__(self, repo):
        super().__init__(datetime.datetime.now(), repo)

    def get_save_data(self):
        return { 'thread_id': 'new' }

    def __str__(self):
        return '{}()'.format(self.__class__.__name__)

    def _submit_extra_args(self):
        return {}

    def submit(self, mr, new_item_callback, callback_args):
        editor = self.get_comment_editor()
        if editor.draft.state:
            return False
        text = editor.text
        blocking = editor.blocking.state
        if not text:
            if blocking:
                # We can't submit an empty comment and we can't just block
                # a MR without a thread. Just keep this comment unsubmitted.
                return False
            # An empty non-blocking comment; just delete it.
            self._remove_comment_editor(self)
            return False
        thread = mr.new_thread(body=text, blocking=blocking, **self._submit_extra_args())
        self._replace_comment_editor(thread, new_item_callback, callback_args)
        return True

    def create_comment_editor(self, focus_widget, inactive_map):
        edit = self._create_comment_editor(inactive_map)
        widget = tuimatic.TextPile([self._new_sep(), edit])
        widget.comment_handler = self
        self.widget_ref = weakref.ref(widget)
        return edit.contents[1][0], widget

    def _init_new_comment_handler(self, handler):
        return

    def _replace_comment_editor(self, thread, new_item_callback, callback_args):
        pile = self._get_widget()
        editor = self.get_comment_editor()
        assert editor
        new_comment_handler = ThreadActivityItem(thread, self.repo)
        self._init_new_comment_handler(new_comment_handler)
        pile.comment_handler = new_comment_handler
        new_comment_handler.widget_ref = weakref.ref(pile)
        pile.contents.clear()
        pile.extend(new_comment_handler.format())
        tuimatic.emit_signal(self, 'changed', self, False)
        new_item_callback(new_comment_handler, *callback_args)
        # mark the edit widget as deleted and trigger its 'changed' signal
        editor.deleted = True
        editor.set_text('')


class NewCommitActivityItem(NewThreadActivityItem):
    def __init__(self, commit, line_pos, repo):
        super().__init__(repo)
        self.set_commit_ref(commit)
        self.line_pos = line_pos

    def get_save_data(self):
        data = super().get_save_data()
        commit = self.get_commit()
        data['commit'] = commit.sha
        if commit.upstream:
            data['upstream'] = commit.upstream[0]
        data['subject'] = commit.name
        data['position'] = self._get_position_arg()
        return data

    def __str__(self):
        commit = self.get_commit()
        return '{}({})'.format(self.__class__.__name__, commit.sha if commit else '???')

    def _get_position_arg(self):
        return { k: v for k, v in zip(('old_path', 'new_path', 'old_line', 'new_line'),
                                      self.line_pos)
                      if v is not None }

    def _submit_extra_args(self):
        commit = self.get_commit()
        pos = self._get_position_arg()
        parent_sha = str(commit.git_commit.parent_ids[0])
        pos['base_sha'] = parent_sha
        pos['start_sha'] = parent_sha
        pos['head_sha'] = commit.backend_sha
        pos['position_type'] = 'text'
        return { 'commit_id': commit.backend_sha, 'position': pos }

    def _init_new_comment_handler(self, handler):
        handler.set_commit_ref(self.get_commit())

    def show_commit_info(self, enabled):
        widget = self._get_widget()
        if not widget:
            return
        widget.contents[1][0].set_enabled(0, enabled)

    def create_comment_editor(self, focus_widget, inactive_map):
        commit = self.get_commit()
        cf = diff_quote_formatter.get_commit_formatter(self.repo, str(commit.git_commit.oid))
        diff_quote = diff_quote_formatter.format(cf, commit, *self.line_pos)
        pile = self._create_comment_editor(inactive_map)
        pile.insert(0, mr_ui.DiffQuote(diff_quote))
        pile.set_enabled(0, False)
        widget = tuimatic.TextPile([self._new_sep(), pile])
        widget.comment_handler = self
        self.widget_ref = weakref.ref(widget)
        diff_widget = commit.get_widget('diff')
        if diff_widget:
            diff_widget.add_thread(widget, True, *self.line_pos)
        return pile.contents[-1][0], widget

    def _remove_comment_editor(self):
        commit = self.get_commit()
        widget = self._get_widget()
        super()._remove_comment_editor()
        if self.widget_ref is None:
            # the widget got deleted by super()._remove_comment_editor
            diff_widget = commit.get_widget('diff')
            if diff_widget:
                diff_widget.delete_thread(widget)


class CommitCommentHandler(CommentHandler):
    def __init__(self, commit, repo):
        super().__init__(datetime.datetime.now(), repo)
        self.set_commit_ref(commit)

    def __str__(self):
        commit = self.get_commit()
        return '{}({})'.format(self.__class__.__name__, commit.sha if commit else '???')

    def get_comment_editor(self, focus_widget=None):
        if focus_widget is None:
            return None
        line_pos = focus_widget.get_line_pos_at_cursor()
        if line_pos is None:
            return None
        for widget in focus_widget.get_threads(*line_pos):
            if isinstance(widget.comment_handler, NewCommitActivityItem):
                return widget.contents[-1][0].contents[-1][0]
        return None

    def create_comment_editor(self, focus_widget, inactive_map):
        commit = self.get_commit()
        if focus_widget is None or commit is None:
            return None, None
        line_pos = focus_widget.get_line_pos_at_cursor()
        if line_pos is None:
            return None, None
        if line_pos[2] is None and line_pos[3] is None:
            # can't add a comment at the file header
            return None, None

        new_handler = NewCommitActivityItem(commit, line_pos, self.repo)
        return new_handler.create_comment_editor(focus_widget, inactive_map)


class LabelActivityItem(BaseActivityItem):
    def __init__(self, label_data):
        self.label_data = label_data
        super().__init__(label_data['created_at'])

    def format(self):
        content = []
        content.append(('desc-sys', '{}, '
                                    .format(formatters.format_timestamp(self.timestamp))))
        content.extend(formatters.format_name(self.label_data['user'], system=True))
        action = self.label_data['action']
        if action == 'add':
            action = 'added'
        elif action == 'remove':
            action = 'removed'
        label = (self.label_data['label']['name'] if self.label_data['label']
                 else 'a deleted label')
        content.append(('desc-sys', ' {} {}\n'.format(action, label)))
        return [self._new_sep(),
                self._create_text(content, system=True, always=False)]


class VersionActivityItem(BaseActivityItem):
    def __init__(self, version):
        self.version_number = version.number
        super().__init__(version.created_at)

    def format(self):
        content = []
        content.append(('desc-sys', '{}, pushed '
                                    .format(formatters.format_timestamp(self.timestamp))))
        content.append(('desc-version', 'v{}'.format(self.version_number)))
        content.append(('desc-sys', '\n'))
        return [self._new_sep(),
                self._create_text(content, system=True, always=True)]


class Activity:
    def __init__(self, repo, mr, version_getters):
        self.general_comment_handler = NewThreadActivityItem(repo)

        self.commit_data = {}
        self.data = []
        for thread in mr.discussions():
            item = ThreadActivityItem(thread, repo)
            self.data.append(item)
            self._connect_changed(item)
            commit_id = item.get_commit_id()
            if commit_id:
                self.commit_data.setdefault(commit_id, []).append(item)

        self.data.extend(LabelActivityItem(e) for e in mr.resource_label_events())
        self.data.extend(VersionActivityItem(v) for v in version_getters if v.number > 1)
        self.data.sort(key=lambda x: x.timestamp)
        self.show_all = True

        self.cur_commit_info = True
        self.widget = tuimatic.TextPile([])

    def _connect_changed(self, item):
        tuimatic.connect_signal(item, 'changed', self._item_changed)

    def _disconnect_changed(self, item):
        tuimatic.disconnect_signal(item, 'changed', self._item_changed)

    def count_blocking_threads(self):
        return sum(1 for item in self.data if item.is_blocking())

    def get_last_approval(self, user_id):
        last_version = 1
        result = None
        for item in self.data:
            if isinstance(item, VersionActivityItem):
                last_version = item.version_number
            elif isinstance(item, ThreadActivityItem):
                for note in item.thread['notes']:
                    if note['system'] and note['author']['id'] == user_id:
                        if note['body'] == 'approved this merge request':
                            result = last_version
                        elif note['body'] == 'unapproved this merge request':
                            result = None
        return result

    def build_widgets(self, commits):
        for commit in commits:
            if not commit.backend_sha:
                continue
            threads = self.get_commit_threads(commit.backend_sha)
            if threads:
                commit.set_mark(commits.num_marks - 1, '✉')
            for item in threads:
                item.set_commit_ref(commit)
                pos = item.get_commit_position()
                widget = item.get_widget()
                diff_widget = commit.get_widget('diff')
                if diff_widget:
                    diff_widget.add_thread(widget, False, *pos)

        text_list = []
        for item in self.data:
            text_list.append(item.get_widget())
        self.widget.extend(text_list)

    def get_commit_threads(self, sha):
        return self.commit_data.get(sha, ())

    def show(self, show_all=None):
        if show_all is not None:
            self.show_all = show_all
        need_sep = True
        for pile in self.widget.contents:
            # The contents is a list of TextPiles. Each thread is a separate
            # pile. Inside a single thread pile, there are separators and
            # view widgets, alternating. The pile always starts with
            # a separator.
            pile = pile[0]
            contents = pile.contents
            # Check whether everything visible in this thread is a system
            # note, whether it's a blocking thread and whether it has
            # a comment editor active.
            # Blocking threads and threads with active comment editor are
            # always visible.
            blocking = contents[0][0].blocking
            thread_show_all = self.show_all or blocking
            system = system_all = not blocking
            i = 1
            while i < len(contents):
                w = contents[i][0]
                if w.is_comment_editor:
                    thread_show_all = True
                    # also need to set system = system_all = False but comment
                    # editor has also is_system = False, so this is covered
                if w.show_always:
                    system = system and w.is_system
                system_all = system_all and w.is_system
                i += 2
            if thread_show_all:
                system = system_all
            need_sep = need_sep or not system
            # Show/hide the widgets.
            i = 1
            thread_start = True
            while i < len(contents):
                w = contents[i][0]
                show = w.show_always or thread_show_all
                pile.set_enabled(i, show)
                if show and need_sep and thread_start:
                    # The first visible note in the thread does not come
                    # with its preceeding separator but with the very first
                    # separator in the pile. The preceeding separator is
                    # likely a minor sep but we need a major sep; plus, the
                    # blocking status is visible only in the first
                    # separator.
                    pile.set_enabled(0, True)
                    if i != 1:
                        pile.set_enabled(i - 1, False)
                    thread_start = False
                    need_sep = not w.is_system
                elif show:
                    show_sep = need_sep or not w.is_system
                    pile.set_enabled(i - 1, show_sep)
                    need_sep = not w.is_system
                else:
                    pile.set_enabled(i - 1, False)
                i += 2

    def toggle(self):
        self.show(not self.show_all)

    def show_commit_info(self, enabled):
        self.cur_commit_info = enabled
        for item in self.data:
            if isinstance(item, CommentHandler):
                item.show_commit_info(enabled)

    def _item_changed(self, item, remove):
        if remove:
            indices = []
            for i, pile in enumerate(self.widget.contents):
                if getattr(pile[0], 'comment_handler', None) == item:
                    indices.append(i)
            for i in reversed(indices):
                del self.widget.contents[i]
            self.data.remove(item)
            self._disconnect_changed(item)
        self.show()

    def find_thread(self, thread_id):
        for item in self.data:
            if isinstance(item, ThreadActivityItem) and item.thread_id == thread_id:
                return item
        raise ThreadNotFoundError(thread_id)

    def add_comment_editor(self, comment_handler, focus_widget, inactive_map):
        editor = comment_handler.get_comment_editor(focus_widget)
        if editor:
            # there is already a new comment, continue editing it
            return editor, False
        editor, new_widget = comment_handler.create_comment_editor(focus_widget, inactive_map)
        if not editor:
            return None, False
        if new_widget:
            assert(new_widget.comment_handler not in self.data)
            self.data.append(new_widget.comment_handler)
            self.widget.append(new_widget)
            self._connect_changed(new_widget.comment_handler)
        self.show()
        return editor, True

    def get_items_with_comment_editor(self):
        return [(item, i) for i, item in enumerate(self.data)
                if isinstance(item, CommentHandler) and item.get_comment_editor()]

    def preview_comments(self):
        widgets = []
        for item, _ in self.get_items_with_comment_editor():
            widget = item.preview(len(widgets) > 0)
            if widget is None:
                continue
            widgets.append(widget)
        return tuimatic.TextBox(widgets)

    def add_commit_data_item(self, new_item, index):
        self._connect_changed(new_item)
        self.data[index] = new_item
        new_item.show_commit_info(self.cur_commit_info)

        commit_id = new_item.get_commit_id()
        if commit_id:
            self.commit_data.setdefault(commit_id, []).append(new_item)

    def submit_comments(self, mr):
        successful = 0
        last_error = None
        for item, i in self.get_items_with_comment_editor():
            try:
                if item.submit(mr, self.add_commit_data_item, (i,)):
                    successful += 1
            except gitlab.BadRequestError as e:
                last_error = e
        if last_error:
            last_error.partially_successful = successful > 0
            raise last_error
        # True indicates that the MR should be optionally re-added to the
        # TODO list
        return successful > 0
