import tuimatic

from modules.settings import settings
from modules.utils import lab
from modules import (
    gitlab,
    mr,
    packages,
    screen,
    ui,
    utils,
)

class BaseScreen(screen.BaseScreen):
    name = ''
    full_name = ''
    config_section = None
    config_section_display_name = None
    default_sort = None
    available_sort = ['mr_created', 'mr_updated', 'mr_id', 'branch', 'title', 'author']
    display_date = 'mr_created'
    include_time = False
    todo_action = True

    @classmethod
    def register_settings(cls):
        if not cls.config_section:
            return
        sect = settings.section(cls.config_section, cls.config_section_display_name)
        sorts = []
        for c in cls.__mro__:
            for s in getattr(c, 'available_sort', ()):
                if s in sorts:
                    continue
                sorts.append(s)
                sorts.append('reverse ' + s)
        sect.register('sort-by', tuple(sorts), default=cls.default_sort,
                      display_name='Sort order')

    def add_actions(self, actions):
        return

    def init_ui(self):
        self.display_all = False
        self.widgets.mr_list = tuimatic.ListBox()

        actions = [
            { 'name': 'open', 'prio': 3, 'help': 'open' },
            { 'name': 'gitlab', 'prio': 3, 'help': 'open in GitLab' },
            { 'name': 'all-mrs', 'prio': 2, 'help': 'all MRs',
              'long_help': 'show all/show supported merge requests' },
            { 'name': 'refresh', 'prio': 2, 'help': 'refresh',
              'confirm': 'Do you want to refresh?' },
            { 'name': 'quit', 'prio': 0, 'help': self.app.get_quit_text(),
              'confirm': 'Do you want to quit?' },
            { 'name': 'next-item', 'prio': 1, 'long_help': 'next item' },
            { 'name': 'prev-item', 'prio': 1, 'long_help': 'previous item' },
        ]
        if self.todo_action:
            actions.append({ 'name': 'todo-add', 'prio': 2, 'help': 'todo',
                             'long_help': 'add this merge request to the TODO list' })
        self.add_actions(actions)

        self.action_tracker.add_actions(self.widgets.mr_list, actions)

        return self.widgets.mr_list

    def load_extra_data(self):
        return

    def load_data(self):
        popup = self.app.wait_popup('Loading {}...'.format(self.full_name))
        self.load_extra_data()
        # get the actual list
        self.enable_all = None
        mrs = self.fetch_list()
        reverse = self.sort_list(mrs)
        popup.stop()

        if self.enable_all is None:
            # if fetch_list() did not set enable_all, look whether there are
            # some items that are not displayed by default
            self.enable_all = any(not mr['known'] for mr in mrs)
        self.action_tracker.set_action_enabled('all-mrs', self.enable_all)

        self.walker = ui.SmartListWalker(mrs, self.app.focus_map, self.app.passive_map,
                                         formatter=self.format_item)
        del_dir = settings['lists']['delete-direction']
        self.walker.set_direction_after_delete(del_dir == 'down' or
                                               (not reverse and del_dir == 'newer') or
                                               (reverse and del_dir == 'older'))
        self.walker.set_filter(self.mr_filter)
        self.widgets.mr_list.body = self.walker
        self.update()

    def sort_list(self, mrs):
        if not self.config_section:
            return
        sort_key = settings[self.config_section]['sort-by']
        if not sort_key:
            return

        reverse = False
        if 'reverse' in sort_key:
            sort_key = sort_key.replace('reverse', '').strip()
            reverse = True

        try:
            mrs.sort(key=lambda item: item[sort_key], reverse=reverse)
        except KeyError:
            raise screen.ScreenError('Invalid sort key specified in the config file')
        return reverse

    def fetch_list(self):
        raise NotImplementedError

    def is_known(self, pkg, data, mr):
        return bool(pkg)

    def parse_mr(self, pkg, obj, data, project_name):
        """Parse and return the given merge request. It will be added to the
        internal list of MRs to display. If 'pkg' is a package or True, the
        MR will be shown. If 'pkg' is False (or None), the MR will be hidden
        by default and an action to show/hide also the hidden MRs will be
        added."""
        flags = []
        if data.get('state', 'opened') != 'opened':
            flags.append(data['state'])
        if data.get('work_in_progress'):
            flags.append('draft')

        my_id = lab.me['id']
        info_flags = []
        if any(u['id'] == my_id for u in data.get('assignees', ())):
            info_flags.append('assigned')

        labels = []
        if isinstance(pkg, packages.Package) and 'labels' in data:
            for label in pkg.filter_list_labels(data['labels'], flags):
                color = pkg.get_label_color(label)
                if color is None:
                    continue
                labels.append((color, label))

        mr = { 'obj': obj,
               'url': data['web_url'],
               'mr_created': gitlab.parse_datetime(data['created_at']),
               'mr_updated': gitlab.parse_datetime(data['updated_at']),
               'mr_id': data['iid'],
               'branch': '{}/{}'.format(project_name, data['target_branch']),
               'title': utils.oneline(data['title']),
               'author': data['author']['name'],
               'flags': flags,
               'info_flags': info_flags,
               'labels': labels,
             }
        mr['known'] = self.is_known(pkg, data, mr)
        return mr

    def format_item(self, item):
        result = []
        result.append(('list-date', '{:%Y-%m-%d}'.format(item[self.display_date])))
        result.append(('list', ' '))
        if item.get('who'):
            result.append(('list-person', item['who']))
            result.append(('list', ' '))
        if item.get('action'):
            result.append(('list', '{} '.format(item['action'])))
        result.append(('list-sha', item['branch']))
        result.append(('list-id', '!{}'.format(item['mr_id'])))
        if item['author']:
            result.append(('list', ' '))
            result.append(('list-person', item['author']))
        result.append(('list', ' {}'.format(item['title'])))
        line2 = []
        if self.include_time:
            t = item[self.display_date]
            if t.strftime('%p'):
                # 12-hour format
                t = ('list-time', '{:%I:%M%p}'.format(t))
            else:
                t = ('list-time', '{:%H:%M}'.format(t))
            line2.append(t)
            line2.append(('list', ' ' * (10 - len(t[1]))))
        if item['flags']:
            line2.append(('list-error', ' '.join(item['flags'])))
        if item['info_flags']:
            if line2:
                line2.append(('list', ' '))
            line2.append(('list-info', ' '.join(item['info_flags'])))
        for color, label in item['labels']:
            if line2:
                line2.append(('list', ' '))
            line2.append(('list-label{}'.format('-' + color if color else ''), label))
        if item.get('body'):
            if line2:
                line2.append(('list', ' '))
            line2.append(('list-comment', item['body']))
        if self.include_time:
            result.append(('list', '\n'))
        else:
            result.append(('list', '\n           '))
        result.extend(line2)
        return result

    def mr_filter(self, mr):
        return self.display_all or mr['known']

    def update_actions(self):
        self.action_tracker.set_action_enabled('open', self.mr_count > 0)
        self.action_tracker.set_action_enabled('gitlab', self.mr_count > 0)

    def update(self):
        self.mr_count = sum(1 for _ in self.walker.objects())
        self.update_actions()
        self.app.update_help()
        title = [
            ('header', ' {} list '.format(utils.capitalize(self.name))),
            ('header-commits', ' {} items '.format(self.mr_count)),
            ('header', ' {} '.format(lab.me.full_user())),
        ]
        self.app.set_header(title)
        self.walker.reapply_filter()

    def switch_list(self):
        self.display_all = not self.display_all
        self.update()

    def del_mr(self, index):
        del self.walker[index]
        self.update()

    def open_mr(self, browser=False):
        url = self.walker.get_focused_object()['url']
        if browser:
            self.app.web_browser(url)
        else:
            self.app.start_screen(mr.Screen, mr_id=url)

    def _add_todo(self, mr):
        popup = self.app.wait_popup('Working...')
        try:
            mr.todo()
        except gitlab.NotModifiedError:
            popup.stop()
            self.app.message_popup(None, 'Already on the TODO list.')
            return
        self.app.add_to_history(popup, mr, 'added to TODO list')
        popup.stop()

    def add_todo(self):
        self._add_todo(self.walker.get_focused_object()['obj'])

    def scroll(self, amount):
        pos = self.widgets.mr_list.focus_position
        try:
            if amount > 0:
                self.widgets.mr_list.focus_position = self.walker.next_position(pos)
            else:
                self.widgets.mr_list.focus_position = self.walker.prev_position(pos)
        except IndexError:
            pass

    def action(self, what, widget, size):
        if what == 'quit':
            self.app.quit()
        elif what == 'next-item':
            self.scroll(1)
        elif what == 'prev-item':
            self.scroll(-1)
        elif what == 'refresh':
            self.load_data()
        elif what == 'open':
            self.open_mr()
        elif what == 'gitlab':
            self.open_mr(browser=True)
        elif what == 'todo-add':
            self.add_todo()
        elif what == 'all-mrs':
            self.switch_list()
        else:
            return False
        return True


class BaseApprovalsScreen(BaseScreen):

    @classmethod
    def add_arguments(cls, parser):
        super().add_arguments(parser)
        parser.add_argument('-A', '--hide-approved', action='store_true',
                            help='hide the merge requests approved by you')

    def __init__(self, app, args):
        super().__init__(app, args)
        self.hide_approved = vars(args).get('hide_approved', False)

    def load_extra_data(self):
        # get my approvals
        self.mrs_approved = set(mr['id']
                                for mr in lab.mrs(view='simple', scope='all', state='opened',
                                                  approved_by_ids=[lab.me['id']]))

    def is_known(self, pkg, data, mr):
        approved = data['id'] in self.mrs_approved
        if approved:
            mr['info_flags'].append('approved by you')
        return bool(pkg) and not (approved and self.hide_approved)
