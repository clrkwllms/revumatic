import fnmatch
import io
import re
import stat
import time
import yaml
from modules.settings import settings
from modules.utils import lab
from modules import connection

class OwnersError(Exception):
    pass

class Owners:
    cached_file = 'kernel-owners.yaml'

    def __init__(self, max_age):
        # max_age is in hours
        st = settings.dir.exists(self.cached_file)
        if not st:
            download = True
        elif stat.S_ISLNK(st.st_mode):
            # do not overwrite a symlink; this allows the user to provide
            # a custom owners.yaml by symlinking to it
            download = False
        elif st.st_mtime + max_age * 3600 < time.time():
            download = True
        else:
            download = False
        if download:
            try:
                self._download()
            except connection.HTTPError as e:
                if not st:
                    raise OwnersError(connection.error_reason(e))
                # just use the outdated cached version if we have it
                download = False
        if not download:
            with settings.dir.open(self.cached_file) as f:
                self._parse(f)

    def _download(self):
        pool = connection.https_pool('gitlab.com')
        resp = pool.request('GET', '/redhat/centos-stream/src/kernel/documentation/-/raw/main/info/owners.yaml',
                            fields={'inline': 'false'})
        data = resp.data.decode('utf-8', errors='replace')
        with settings.dir.rewrite(self.cached_file) as f:
            f.write(data)
        self._parse(io.StringIO(data))

    def _parse(self, stream):
        try:
            self.data = yaml.safe_load(stream)
        except yaml.YAMLError as e:
            raise OwnersError('Cannot parse owners.yaml: {}'.format(e))

    def _subsystem_entries(self, label):
        for entry in self.data['subsystems']:
            if entry.get('labels', {}).get('name') == label:
                yield entry

    def _path_entries(self, label):
        for entry in self._subsystem_entries(label):
            if entry.get('paths'):
                yield entry['paths']

    def _get_paths(self, label, glob_key, regex_key):
        result = set()
        for paths in self._path_entries(label):
            if paths.get(glob_key):
                result.update(r'\A' + fnmatch.translate(p + '*' if p.endswith('/') else p)
                              for p in paths[glob_key])
            if paths.get(regex_key):
                result.update(paths[regex_key])
        if not result:
            return None
        return '|'.join(result)

    def get_includes(self, subsys):
        result = self._get_paths(subsys, 'includes', 'includeRegexes')
        return result

    def get_excludes(self, subsys):
        result = self._get_paths(subsys, 'excludes', 'excludeRegexes')
        return result

    def get_subsystems(self, files):
        user = lab.me['username']
        # first, collect all subsystems and record whether the user is
        # a maintainer/reviewer
        subs = {}
        for entry in self.data['subsystems']:
            label = entry.get('labels', {}).get('name')
            if not label:
                continue
            if label == 'redhat':
                # special case the weird "redhat" entries
                name = 'Red Hat Build Infrastructure'
            else:
                name = ' '.join(map(str.capitalize, entry.get('subsystem', label).split()))
            cnt = sum(m.get('gluser') == user for m in entry.get('maintainers', ()))
            cnt += sum(m.get('gluser') == user for m in entry.get('reviewers', ()))
            if label in subs:
                cnt += subs[label][1]
            subs[label] = (name, cnt)
        # second, filter out the subsystems that are not touched and store
        # separately the ones the user is maintaining/reviewing
        ours = {}
        rest = {}
        for label, (name, cnt) in subs.items():
            incl = self.get_includes(label)
            if incl is None:
                continue
            incl = re.compile(incl)
            excl = self.get_excludes(label)
            if excl is not None:
                excl = re.compile(excl)
            for path in files:
                if not incl.search(path):
                    continue
                if excl and excl.search(path):
                    continue
                if cnt:
                    ours[label] = ('list-item-important', name)
                else:
                    rest[label] = name
                break
        # sort and merge the lists
        ours = sorted(ours.items(), key=lambda v: (v[1], v[0]))
        rest = sorted(rest.items(), key=lambda v: (v[1], v[0]))
        ours.append((None, ('list-item-important', 'clear the filter')))
        ours.extend(rest)
        return ours
