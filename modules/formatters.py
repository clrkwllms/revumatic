import collections
import datetime
import difflib
import itertools
import re

from modules.settings import settings
from modules.utils import lab
from modules import gitlab


def commit_name(commit):
    try:
        return commit.message[:commit.message.index('\n')]
    except ValueError:
        return commit.message


def format_name(user, email=None, system=False):
    if user is None:
        return [('desc-sys' if system else 'desc-person', 'A deleted user')]
    if user['id'] == lab.me['id']:
        cls1 = cls2 = 'me'
    elif system:
        cls1 = cls2 = 'sys'
    else:
        cls1 = 'person'
        cls2 = 'username'
    result = [('desc-' + cls1, user['name']),
                ('desc-' + cls2, ' @' + user['username'])]
    if email:
        result.insert(1, ('desc-' + cls1, ' <{}>'.format(email)))
    return result


def format_timestamp(timestamp):
    if isinstance(timestamp, str):
        timestamp = gitlab.parse_datetime(timestamp)
    return '{:%Y-%m-%d %H:%M}'.format(timestamp)


def replace_trail(txt):
    if txt.endswith(' '):
        txt = txt[:-1] + settings.format_trail[0]
    return txt


def join_lines(lines):
    return '\n'.join(itertools.chain(lines, ('',)))


class DiffList(collections.UserList):
    def __init__(self, store_highlights=False, store_line_pos=False):
        super().__init__()
        if store_highlights:
            self.x = self.y = 0
            self.highlights = []
            self.last_highlighted = False
        else:
            self.highlights = None
        self.line_pos_map = [] if store_line_pos else None

    def _add_chars(self, item, highlighted, line_pos=None):
        s = item if type(item) == str else item[1]
        if s == '':
            return

        if self.highlights is not None:
            record_end = next_y = self.y + s.count('\n')
            if next_y == self.y:
                # no newline
                next_x = self.x + len(s)
            else:
                next_x = len(s) - s.rindex('\n') - 1
                if s.endswith('\n'):
                    # don't make the highlight spawn to the next line if it ends
                    # with \n
                    record_end -= 1

            if highlighted:
                if self.last_highlighted:
                    self.highlights[-1][1] = record_end
                else:
                    self.highlights.append([self.y, record_end, self.x])
            self.last_highlighted = highlighted

            self.x = next_x
            self.y = next_y
        else:
            assert(not highlighted)
        if self.line_pos_map is not None:
            if s.endswith('\n'):
                assert(s.count('\n') == 1)
                self.line_pos_map.append(line_pos)
            else:
                # line_pos can be specified only with a line end
                assert(line_pos is None)
                assert(s.count('\n') == 0)
        else:
            assert(line_pos is None)

    def append(self, item, highlighted=False, line_pos=None):
        self._add_chars(item, highlighted, line_pos)
        super().append(item)

    def extend(self, items, highlighted=False):
        assert(self.line_pos_map is None)
        for item in items:
            self._add_chars(item, highlighted)
        super().extend(items)

    def lines(self):
        return self.y + 1


def merge_highlights(list1, list2):
    def overlaps(place1, place2):
        if not place1 or not place2:
            return False
        return ((place1[0] >= place2[0] and place1[0] <= place2[1]) or
                (place2[0] >= place1[0] and place2[0] <= place1[1]))

    result = []
    h1 = list1.highlights
    h2 = list2.highlights
    pos1 = pos2 = 0
    while True:
        cur1 = cur2 = None
        if pos1 < len(h1):
            cur1 = h1[pos1]
        if pos2 < len(h2):
            cur2 = h2[pos2]
        if not cur1 and not cur2:
            break
        if overlaps(cur1, cur2):
            # areas overlap
            result.append((cur1[2], cur1[0], cur2[2], cur2[0]))
            pos1 += 1
            pos2 += 1
            continue
        # areas don't overlap, try to match to the previous area
        if pos2 > 0:
            candidate = h2[pos2 - 1]
            if overlaps(cur1, candidate):
                result.append((cur1[2], cur1[0], candidate[2], candidate[0]))
                pos1 += 1
                continue
        if pos1 > 0:
            candidate = h1[pos1 - 1]
            if overlaps(candidate, cur2):
                result.append((candidate[2], candidate[0], cur2[2], cur2[0]))
                pos2 += 1
                continue
        # still no overlap, record the lower position
        if not cur2 or (cur1 and cur1[0] < cur2[0]):
            result.append((cur1[2], cur1[0], cur1[2], cur1[0]))
            pos1 += 1
            continue
        if not cur1 or (cur2 and cur2[0] < cur1[0]):
            result.append((cur2[2], cur2[0], cur2[2], cur2[0]))
            pos2 += 1
            continue
        assert False
    return result


class AnnotationFormatter:
    def __init__(self, default_cls):
        self._default_cls = default_cls
        self._lines = []
        self._line_in_progress = False
        self._extra_breaks = 0

    def add(self, markup, label=False):
        if self._extra_breaks:
            self._lines.append((self._default_cls, '\n' * self._extra_breaks))
            self._extra_breaks = 0
            self._line_in_progress = False
        if not isinstance(markup, list):
            markup = [markup]
        if self._line_in_progress:
            self._lines.append((self._default_cls, ' '))
        for i, m in enumerate(markup):
            if type(m) == str:
                cls = self._default_cls
                txt = m
            else:
                cls, txt = m
            if label and i == 0:
                txt = '\u00a0' + txt
            if label and i == len(markup) - 1:
                txt = txt + '\u00a0'
            self._lines.append((cls, txt))
        self._line_in_progress = True

    def add_break(self):
        """Will not be added if it's already there."""
        if self._line_in_progress:
            self._lines.append((self._default_cls, '\n'))
            self._line_in_progress = False

    def add_empty_line(self):
        """Will not be added at the beginning and end of the annotations."""
        if not self._lines:
            return
        self.add_break()
        self._extra_breaks += 1

    def format(self):
        if not self._lines:
            return ''
        return self._lines + [(self._default_cls, '\n')]


class CommitFormatter:
    def __init__(self, commit, diff, compared_sha=None, compared_titles=None):
        self._commit = commit
        self._diff = diff
        self._compared_titles = compared_titles or ('backport', 'upstream')
        self._compared_sha = compared_sha
        if type(self._compared_sha) == str:
            self._compared_sha = (str(self._commit.oid)[:12], compared_sha[:12])

    def _abbrev(self, commit_id):
        return str(commit_id)[:12]

    def _email(self, sig):
        return '{} <{}>'.format(sig.name, sig.email)

    def _date(self, sig):
        tz = datetime.timezone(datetime.timedelta(minutes=sig.offset))
        return datetime.datetime.fromtimestamp(sig.time, tz)

    def _date_str(self, sig):
        d = self._date(sig)
        weekday = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][d.weekday()]
        month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
                 'Oct', 'Nov', 'Dec'][d.month - 1]
        return '{} {} {} {}'.format(weekday, month, d.day, d.strftime('%H:%M:%S %Y %z'))

    def name(self):
        return commit_name(self._commit)

    def header(self):
        if self._commit is None:
            return self._diff.description
        if isinstance(self._commit, str):
            return self._commit

        msg = self._commit.message
        if msg.endswith('\n'):
            msg = msg[:-1]
        msg = re.sub('^', '    ', msg, flags=re.MULTILINE)

        merge = ''
        if len(self._commit.parent_ids) > 1:
            merge = 'Merge: {}\n'.format(' '.join([self._abbrev(p) for p in self._commit.parent_ids]))

        return 'commit {}\n{}Author: {}\nDate:   {}\n\n{}'.format(self._commit.id, merge,
               self._email(self._commit.author), self._date_str(self._commit.author), msg)

    def get_stat(self):
        files = len(self._diff)
        if not files:
            return None

        stat = {}
        stat['name_len'] = max(len(f.new_name) if f.old_null else len(f.name)
                               for f in self._diff)
        stat['max_changes'] = self._diff.max_changes

        stat['data'] = []
        total_add = 0
        total_del = 0
        for f in self._diff:
            data = {
                'name': f.new_name if f.old_null else f.name,
                'new_name': None,
                'changes': f.changes,
                'added': f.added,
                'removed': f.removed,
            }
            if f.name != f.new_name and not f.old_null and not f.new_null:
                data['new_name'] = f.new_name

            stat['data'].append(data)
            total_add += f.added
            total_del += f.removed

        total = ' {} file{} changed'.format(files, 's' if files > 1 else '')
        if total_add:
            total += ', {} insertion{}(+)'.format(total_add, 's' if total_add > 1 else '')
        if total_del:
            total += ', {} deletion{}(+)'.format(total_del, 's' if total_del > 1 else '')
        stat['total'] = total
        return stat

    @staticmethod
    def format_stat(stat, width=80):
        def clip_path(path, path_len):
            if len(path) <= path_len:
                return path
            return path[:4] + '…' + path[len(path)-path_len+5:]

        name_len = min(stat['name_len'], width * 2 // 3 - 2)
        change_len = len(str(stat['max_changes']))
        stat_len = width - 5 - name_len - change_len
        if stat['max_changes'] == 0:
            unit = 0
        else:
            unit = min(stat_len / stat['max_changes'], 1)

        result = []
        for data in stat['data']:
            name = clip_path(data['name'], name_len)
            line = ' {:{}} | {:-{}} '.format(name, name_len, data['changes'], change_len)
            if data['added']:
                line += '+' * max(int(data['added'] * unit), 1)
            if data['removed']:
                line += '-' * max(int(data['removed'] * unit), 1)
            result.append(line)
            if data['new_name'] is not None:
                new_name = clip_path(data['new_name'], width - 5 - name_len)
                result.append(' {:{}} => {}'.format('', name_len, new_name))

        result.append(stat['total'])
        return result

    def format_header(self):
        return [('desc', self.header() + '\n')]

    def format_diff(self):
        """Returns a tuple of (markup, line_pos_map), where line_pos_map is
        a list of tuples (name, name, old_pos, new_pos), one for each line
        in markup."""
        result = DiffList(store_line_pos=True)
        self._add_diff(result)
        return result.data, result.line_pos_map

    def format_diff_line(self, path, line_no, old, count=3):
        lines = self._diff.get_line(path, line_no, old, count)
        if not lines:
            return None
        result = DiffList()
        for line in lines:
            self._add_line(result, line)
        return result.data

    def get_file_list(self):
        result = []
        for f in self._diff:
            if not f.old_null:
                result.append(f.name)
            if f.name != f.new_name and not f.new_null:
                result.append(f.new_name)
        result.sort()
        return result

    def _add_file_header(self, result, f, line_pos=None):
        for hdr in f.headers:
            result.append(('diff-file', hdr + '\n'))
        result.append(('diff-file', '--- {}\n'.format('/dev/null' if f.old_null else 'a/'+f.name)),
                      line_pos=line_pos)
        result.append(('diff-file', '+++ {}\n'.format('/dev/null' if f.new_null else 'b/'+f.new_name)),
                      line_pos=line_pos)

    def _add_hunk_header(self, result, h):
        parts = h.header.split('@@')
        func = parts[2]
        parts[2] = ''
        result.append(('diff-hunk', '@@'.join(parts)))
        result.append(('diff-func', func + '\n'))

    def _add_line(self, result, line, line_pos=None):
        if line.origin == '+':
            cls = 'diff-add'
        elif line.origin == '-':
            cls = 'diff-del'
        else:
            cls = 'diff'
        result.append((cls, '{}{}\n'.format(line.origin, replace_trail(line.content))),
                      line_pos=line_pos)

    def _add_partial_line(self, highlight, highlight_origin,
                          result, line, start, count=None):
        if count is not None and count <= 0:
            return

        if line.origin == '+':
            origin_cls = 'diff-add'
        elif line.origin == '-':
            origin_cls = 'diff-del'
        else:
            origin_cls = 'diff'
        cls = origin_cls
        if highlight:
            cls += '-miss'
        if highlight_origin:
            origin_cls += '-miss'
        if count is None or start + count > len(line.content):
            if start == 0:
                result.append((origin_cls, line.origin), highlighted=highlight_origin)
            result.append((cls, '{}\n'.format(replace_trail(line.content[start:]))),
                          highlighted=highlight)
            return
        assert start + count <= len(line.content)
        s2 = line.content[start:start+count]
        if start == 0:
            result.append((origin_cls, line.origin))
        result.append((cls, s2), highlighted=highlight)

    def _add_diff(self, result):
        for f in self._diff:
            f_line_pos = (f.new_name if f.old_null else f.name,
                          f.name if f.new_null else f.new_name,
                          None, None)
            self._add_file_header(result, f, line_pos=f_line_pos)
            for h in f:
                self._add_hunk_header(result, h)
                for line in h:
                    line_pos = (f_line_pos[0],
                                f_line_pos[1],
                                None if line.origin == '+' else line.old_pos,
                                None if line.origin == '-' else line.new_pos)
                    self._add_line(result, line, line_pos=line_pos)
        return result

    def _sync(self, left, right):
        left_lines = left.lines()
        right_lines = right.lines()
        if left_lines < right_lines:
            left.extend((('diff-miss', '⦚\n'),) * (right_lines - left_lines),
                        highlighted=True)
        elif right_lines < left_lines:
            right.extend((('diff-miss', '⦚\n'),) * (left_lines - right_lines),
                         highlighted=True)

    def _compare_left_right(self, left, right, h):
        def get_highlight_origin():
            try:
                return h.left[pos_line[0]].origin != h.right[pos_line[1]].origin
            except IndexError:
                return False

        def output_completed_line(highlight, highlight_origin, where, hh, idx, count):
            if count <= 0:
                return False, count
            remaining = len(hh[pos_line[idx]].content) - offs_line[idx]
            if count < remaining + 1:
                return False, count
            self._add_partial_line(highlight, highlight_origin,
                                   where, hh[pos_line[idx]], offs_line[idx])
            count -= remaining + 1  # the added 1 is for the \n
            pos_line[idx] += 1
            offs_line[idx] = 0
            return True, count

        def output_incomplete_line(highlight, highlight_origin, where, hh, idx, count):
            if count <= 0:
                return count
            self._add_partial_line(highlight, highlight_origin,
                                   where, hh[pos_line[idx]], offs_line[idx], count)
            offs_line[idx] += count
            return 0

        def cond_sync():
            # if we stayed in the middle of the left or right line, we'll
            # have to wait with the sync, we can't insert a break there
            if offs_line[0] == 0 and offs_line[1] == 0:
                self._sync(left, right)

        blocks = difflib.SequenceMatcher(a=join_lines(line.content for line in h.left),
                                         b=join_lines(line.content for line in h.right),
                                         autojunk=False).get_matching_blocks()
        # index of the currently processed line on the left and right
        # side:
        pos_line = [0, 0]
        # current offsets in pos_line:
        offs_line = [0, 0]
        # index of the currently processed block from the SequenceMatcher:
        cur_block_idx = 0
        # the previously processed block:
        last_block = (0, 0, 0)
        # number of characters that needs to be consumed
        x_l = x_r = 0
        while True:
            n_l, n_r, n = blocks[cur_block_idx]
            if n < 5 and cur_block_idx < len(blocks) - 1:
                cur_block_idx += 1
                continue
            x_l += n_l - (last_block[0] + last_block[2])
            x_r += n_r - (last_block[1] + last_block[2])

            repeat_l = repeat_r = True
            while repeat_l or repeat_r:
                hl_origin = get_highlight_origin()
                repeat_l, x_l = output_completed_line(True, hl_origin, left, h.left, 0, x_l)
                repeat_r, x_r = output_completed_line(True, hl_origin, right, h.right, 1, x_r)
            # sync after outputting all differing lines
            cond_sync()

            hl_origin = get_highlight_origin()
            x_l = output_incomplete_line(True, hl_origin, left, h.left, 0, x_l)
            x_r = output_incomplete_line(True, hl_origin, right, h.right, 1, x_r)

            if n == 0:
                break

            x_l += n
            x_r += n
            repeat_l = repeat_r = True
            while repeat_l or repeat_r:
                hl_origin = get_highlight_origin()
                repeat_l, x_l = output_completed_line(False, hl_origin, left, h.left, 0, x_l)
                repeat_r, x_r = output_completed_line(False, hl_origin, right, h.right, 1, x_r)
                # sync after outputting the first matching line; this is
                # necessary if we were not able to sync earlier
                cond_sync()

            hl_origin = get_highlight_origin()
            x_l = output_incomplete_line(False, hl_origin, left, h.left, 0, x_l)
            x_r = output_incomplete_line(False, hl_origin, right, h.right, 1, x_r)

            last_block = blocks[cur_block_idx]
            cur_block_idx += 1

    def format_compared(self):
        if not self._diff.was_compared():
            return None
        left = DiffList(store_highlights=True)
        right = DiffList(store_highlights=True)
        if self._compared_sha:
            left.append(('diff-subject', ' {} '.format(self._compared_titles[0])))
            left.append(('diff-sha', ' {}\n'.format(self._compared_sha[0])))
            right.append(('diff-subject', ' {} '.format(self._compared_titles[1])))
            right.append(('diff-sha', ' {}\n'.format(self._compared_sha[1])))
        for f in self._diff.compared():
            if f.left:
                self._add_file_header(left, f)
            if f.right:
                self._add_file_header(right, f.other_file or f)
            self._sync(left, right)
            for h in f.compared():
                if h.left:
                    self._add_hunk_header(left, h.left)
                if h.right:
                    self._add_hunk_header(right, h.right)
                if h.left and h.right and h.left.fuzz:
                    self._compare_left_right(left, right, h)
                else:
                    if h.left:
                        for line in h.left:
                            self._add_line(left, line)
                    if h.right:
                        for line in h.right:
                            self._add_line(right, line)
                self._sync(left, right)
        return left.data, right.data, merge_highlights(left, right)


class CommentFormatter:
    def __init__(self, pkg):
        self.pkg = pkg

    def format(self, commit_id, name, comment, number=None, upstream=None, prefix=None):
        header = []
        if prefix:
            header.append('[{}]'.format(prefix))
        if number is not None:
            header.append('[{}/{}]'.format(*number))
        if commit_id:
            header.append('`{:.12}`'.format(commit_id))
            header.append('("{}")'.format(name))
        else:
            header.append(name)
        if upstream:
            header.append('[upstream:`{:.12}`]'.format(upstream))
        comment = self.pkg.postformat_comment(comment.rstrip('\n').replace('\n', '\n  '),
                                              True)
        return '- {}\n\n  {}'.format(' '.join(header), comment)

    def format_main(self, comment):
        return self.pkg.postformat_comment(comment.rstrip('\n'), False)

    def join(self, formatted_list):
        return '\n\n'.join(formatted_list)


class DiffQuoteFormatter:
    def __init__(self):
        self.diff_cache = {}

    def get_commit_formatter_from_range(self, repo, base_sha, head_sha):
        cache_key = '{}..{}'.format(base_sha, head_sha)
        if cache_key not in self.diff_cache:
            parsed = repo.get_parsed_diff_range(base_sha, head_sha)
            self.diff_cache[cache_key] = CommitFormatter(None, parsed)
        return self.diff_cache[cache_key]

    def get_commit_formatter(self, repo, sha):
        if sha not in self.diff_cache:
            parsed = repo.get_parsed_diff(sha)
            self.diff_cache[sha] = CommitFormatter(None, parsed)
        return self.diff_cache[sha]

    def format(self, commit_formatter, commit, old_path, new_path, old_line, new_line):
        diff_quote = []
        if commit:
            diff_quote.extend([('diff-commit-ref', 'on commit '),
                                ('diff-sha', commit.get_sha(12)),
                                ('diff-commit-ref', ' ({})'.format(commit.name)),
                                ('desc', '\n')])
        if old_line is not None:
            path = old_path
            line = old_line
            old = True
        else:
            path = new_path
            line = new_line
            old = False
        diff_quote.extend([('diff-func', '{}:{}'.format(path, line)),
                            ('desc', '\n')])
        diff = commit_formatter.format_diff_line(path, line, old, count=1)
        if not diff:
            diff = [('diff', '[code not available]\n')]
        diff_quote.extend(diff)
        return diff_quote
